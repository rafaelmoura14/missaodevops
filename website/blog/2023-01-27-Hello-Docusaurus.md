---
title: Hello Docusauro.io
author: leg1on4rio
authorURL: http://twitter.com/leg1on4rio
authorFBID: 661277173
---

This blog post will test file name parsing issues when periods are present.

<!--truncate-->

## Agora sim!

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum dignissim ultricies. Fusce rhoncus ipsum tempor eros aliquam consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus elementum massa eget nulla aliquet sagittis. Proin odio tortor, vulputate ut odio in, ultrices ultricies augue. Cras ornare ultrices lorem malesuada iaculis. Etiam sit amet libero tempor, pulvinar mauris sed, sollicitudin sapien.

```
drwxr-x--- 25 ti   ti   4,0K jan 27 08:51  .
drwxr-xr-x  3 root root 4,0K jan 18 09:36  ..
drwxr-xr-x  3 ti   ti   4,0K jan 18 11:06  .anydesk
drwxr-xr-x  2 ti   ti   4,0K jan 18 09:54 'Área de Trabalho'
-rw-------  1 ti   ti   3,1K jan 26 10:09  .bash_history
-rw-r--r--  1 ti   ti    220 jan 18 09:36  .bash_logout
-rw-r--r--  1 ti   ti   3,7K jan 18 09:36  .bashrc
drwx------ 22 ti   ti   4,0K jan 27 08:50  .cache
drwx------ 23 ti   ti   4,0K jan 24 15:41  .config
drwxr-xr-x  2 ti   ti   4,0K jan 18 09:54  Documentos
drwxr-xr-x  2 ti   ti   4,0K jan 27 09:11  Downloads
-rw-rw-r--  1 ti   ti     85 jan 24 09:54  .gitconfig
drwx------  3 ti   ti   4,0K jan 18 10:20  .gnome
drwx------  2 ti   ti   4,0K jan 18 16:20  .gnupg
drwxr-xr-x  3 ti   ti   4,0K jan 18 10:41  Imagens
-rw-------  1 ti   ti     20 jan 24 09:49  .lesshst
drwx------  3 ti   ti   4,0K jan 18 09:54  .local
drwxr-xr-x  2 ti   ti   4,0K jan 18 09:54  Modelos
drwxr-xr-x  2 ti   ti   4,0K jan 18 09:54  Música
drwxrwxr-x  4 ti   ti   4,0K jan 23 10:53  .npm
drwx------  3 ti   ti   4,0K jan 18 16:20  .nv
-rw-rw-r--  1 ti   ti   347M nov  9 19:06  nvidia-linux.run
-rw-rw-r--  1 ti   ti   1,1K jan 18 16:22  .nvidia-settings-rc
-rw-r--r--  1 ti   ti    357 jan 18 10:27  .pam_environment
drwx------  3 ti   ti   4,0K jan 18 10:19  .pki
-rw-r--r--  1 ti   ti    807 jan 18 09:36  .profile
drwxr-xr-x  2 ti   ti   4,0K jan 18 09:54  Público
drwxrwxr-x 10 ti   ti   4,0K jan 18 10:03  RTL8811CU
drwxrwxr-x  9 ti   ti   4,0K jan 18 10:11  rtl8821CU
drwx------  6 ti   ti   4,0K jan 18 10:22  snap
drwx------  2 ti   ti   4,0K jan 23 10:21  .ssh
-rw-r--r--  1 ti   ti      0 jan 18 09:55  .sudo_as_admin_successful
drwxr-xr-x  3 ti   ti   4,0K jan 18 11:06  Vídeos
drwxrwxr-x  3 ti   ti   4,0K jan 18 11:03  .vscode
drwxrwxr-x  3 ti   ti   4,0K jan 27 08:51  .yarn
ti@ti-desktop:~$ 


```

